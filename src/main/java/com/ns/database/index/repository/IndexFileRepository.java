package com.ns.database.index.repository;

import com.ns.database.content.model.ContentHeader;
import com.ns.database.index.model.IndexHeader;
import com.ns.database.index.model.IndexRecord;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import lombok.NonNull;

public class IndexFileRepository {

    private final RandomAccessFile file;
    private final IndexHeader header;

    public IndexFileRepository(final String filePath) throws IOException {
        this.file = new RandomAccessFile(filePath, "rw");
        if (file.length() == 0) {
            initHeader();
        }

        this.header = readHeader();
    }

    public long create(@NonNull final long dataPosition) throws IOException {
        final long recordId = header.getLastId() + 1;

        final IndexRecord record = new IndexRecord();
        record.setId(recordId);
        record.setActive(true);
        record.setDataPosition(dataPosition);

        final long nextPosition = writeRecord(record);

        header.setLastId(recordId);
        header.setWritePosition(nextPosition);
        header.setRecordCount(header.getRecordCount() + 1);
        writeHeader(header);

        return recordId;
    }

    public long get(@NonNull final long recordId) throws IOException {
        final long position = findRecordById(recordId);
        final IndexRecord record = readRecord(position);

        return (record != null) ? record.getDataPosition() : -1;
    }

    public void delete(@NonNull final long recordId) throws IOException {
        final long position = findRecordById(recordId);

        if (position < 0) {
            return;
        }

        file.seek(position + Long.BYTES);

        file.writeBoolean(false);
    }

    private IndexHeader readHeader() throws IOException {
        final IndexHeader result = new IndexHeader();

        file.seek(0);

        result.setWritePosition(file.readLong());
        result.setLastId(file.readLong());
        result.setRecordCount(file.readLong());

        return result;
    }

    private void writeHeader(@NonNull final IndexHeader header) throws IOException {
        file.seek(0);

        file.writeLong(header.getWritePosition());
        file.writeLong(header.getLastId());
        file.writeLong(header.getRecordCount());
    }

    private IndexRecord readRecord(@NonNull long position) throws IOException {
        if (position < 0) {
            return null;
        }

        final IndexRecord result = new IndexRecord();

        file.seek(position);

        result.setId(file.readLong());
        result.setActive(file.readBoolean());
        result.setDataPosition(file.readLong());

        return result;
    }

    private long writeRecord(@NonNull final IndexRecord record) throws IOException {
        file.seek(header.getWritePosition());

        file.writeLong(record.getId());
        file.writeBoolean(record.isActive());
        file.writeLong(record.getDataPosition());

        return file.getFilePointer();
    }

    private void initHeader() throws IOException {
        file.writeLong(IndexHeader.BYTES);
        file.writeLong(0);
        file.writeLong(0);
    }

    private long findRecordById(@NonNull final long recordId) throws IOException {
        long result;

        long tmpId;

        long startPosition = IndexHeader.BYTES;
        long endPosition = header.getWritePosition() - IndexRecord.BYTES;

        while (endPosition >= startPosition) {
            result = (endPosition + startPosition) / 2 -
                    (((endPosition + startPosition) % 2 != 0) ? IndexRecord.BYTES / 2 : 0);

            file.seek(result);
            tmpId = file.readLong();

            if (tmpId == recordId) {

                if (file.readBoolean()) {
                    return result;
                } else {
                    return -1;
                }
            }
            if (tmpId < recordId) {
                startPosition = result + IndexRecord.BYTES;
                continue;
            }

            endPosition = result - IndexRecord.BYTES;
        }

        return -1;
    }

    public void compact() throws IOException {
        final long endOfFile = header.getWritePosition();

        long missMatch = 0;
        long previousPosition = 0;
        boolean previousIsDeleted = false;

        long lastId = 0;
        long recordCount = 0;

        long compactPosition = IndexHeader.BYTES;
        long getPosition = IndexHeader.BYTES;

        IndexRecord record;

        while (getPosition < endOfFile) {
            record = readRecord(getPosition);

            if (record.isActive()) {
                recordCount++;

                record.setId(recordCount);
                lastId = record.getId();

                header.setWritePosition(compactPosition);

                if (compactPosition != getPosition) {
                    if(previousIsDeleted){
                        missMatch += record.getDataPosition() - previousPosition;

                        previousIsDeleted = false;
                    }

                    record.setDataPosition(record.getDataPosition() - missMatch);
                    writeRecord(record);
                }

                compactPosition += IndexRecord.BYTES;
            } else {
                previousIsDeleted = true;
                previousPosition = record.getDataPosition();
            }

            getPosition += IndexRecord.BYTES;
        }

        file.setLength(compactPosition);

        header.setLastId(lastId);
        header.setRecordCount(recordCount);
        header.setWritePosition(compactPosition);

        writeHeader(header);
    }
}
