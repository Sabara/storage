package com.ns.database.index.model;

import lombok.Data;

@Data
public class IndexHeader {

  public static final int BYTES = Long.BYTES + Long.BYTES + Long.BYTES;

  private long writePosition;
  private long lastId;
  private long recordCount;

}
