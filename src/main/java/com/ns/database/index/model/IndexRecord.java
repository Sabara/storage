package com.ns.database.index.model;

import lombok.Data;

@Data
public class IndexRecord {

  public static final int BYTES = Long.BYTES + Byte.BYTES + Long.BYTES;

  private long id;
  private boolean active;
  private long dataPosition;

}