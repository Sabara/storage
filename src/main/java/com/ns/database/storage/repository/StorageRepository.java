package com.ns.database.storage.repository;

import com.ns.database.content.repository.ContentFileRepository;
import com.ns.database.index.repository.IndexFileRepository;
import lombok.NonNull;

import java.io.IOException;

public class StorageRepository {

    private IndexFileRepository indexFileRepository;
    private ContentFileRepository contentFileRepository;

    public StorageRepository(final String indexFilePath, final String contentFilePath) throws IOException {
        indexFileRepository = new IndexFileRepository(indexFilePath);
        contentFileRepository = new ContentFileRepository(contentFilePath);
    }

    public void create(@NonNull byte[] data) throws IOException {
        indexFileRepository.create(contentFileRepository.create(data));
    }

    public byte[] get(@NonNull final long recordId) throws IOException {
        return contentFileRepository.get(indexFileRepository.get(recordId));
    }

    public void delete(@NonNull final long recordId) throws IOException {
        indexFileRepository.delete(recordId);
    }

    public void compact() throws IOException {
        indexFileRepository.compact();

        long recordId = 1;
        long position;

        byte[] data;

        do {
            position = indexFileRepository.get(recordId);
            data = contentFileRepository.get(position);

            contentFileRepository.compact(data, position);
            recordId++;
        }while(position != -1);

        contentFileRepository.compact(data == null && recordId == 2);
    }
}
