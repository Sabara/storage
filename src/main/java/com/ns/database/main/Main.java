package com.ns.database.main;

import com.ns.database.storage.repository.StorageRepository;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        StorageRepository repository = new StorageRepository("mystorage.index", "mystorage.data");

        repository.delete(1);
        repository.compact();
        System.out.println(deserialize(repository.get(1)));
    }

    public static byte[] serialize(SomeEntity entity) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(entity);
            return bos.toByteArray();
        }
    }

    public static SomeEntity deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        if(bytes == null){
            return null;
        }

        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return (SomeEntity) in.readObject();
        }
    }
}
