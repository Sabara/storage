package com.ns.database.content.model;

import lombok.Data;

@Data
public class ContentHeader{

    public static final int BYTES = Long.BYTES;

    private long writePosition;

}
