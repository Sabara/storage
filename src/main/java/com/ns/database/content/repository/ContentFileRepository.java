package com.ns.database.content.repository;

import com.ns.database.content.model.ContentHeader;
import lombok.NonNull;

import java.io.IOException;
import java.io.RandomAccessFile;

public class ContentFileRepository {

    private RandomAccessFile file;
    private ContentHeader header;

    public ContentFileRepository(final String filePath) throws IOException {
        this.file = new RandomAccessFile(filePath, "rw");
        if(file.length() == 0){
            initHeader();
        }

        this.header = readHeader();
    }

    public long create(@NonNull final byte[] data) throws IOException {
        final long result = header.getWritePosition();

        file.seek(result);

        file.writeLong(data.length);
        file.write(data);

        header.setWritePosition(file.getFilePointer());
        writeHeader(header);

        return result;
    }

    public byte[] get(@NonNull final long position) throws IOException {
        if(position < 0){
            return null;
        }

        byte[] result;

        file.seek(position);
        result = new byte[(int)file.readLong()];

        file.read(result);

        return result;
    }

    private ContentHeader readHeader() throws IOException {
        final ContentHeader result = new ContentHeader();

        file.seek(0);

        result.setWritePosition(file.readLong());

        return result;
    }

    private void writeHeader(@NonNull final ContentHeader header) throws IOException {
        file.seek(0);

        file.writeLong(header.getWritePosition());
    }

    private void initHeader() throws IOException {
        file.writeLong(ContentHeader.BYTES);
    }

    public void compact(byte[] data, long position) throws IOException {
        if(position == -1 || data == null){
            return;
        }

        header.setWritePosition(position);

        create(data);
    }

    public void compact(boolean allDataDeleted) throws IOException {
        if(allDataDeleted){
            header.setWritePosition(ContentHeader.BYTES);
        }

        file.setLength(header.getWritePosition());
        writeHeader(header);
    }
}
